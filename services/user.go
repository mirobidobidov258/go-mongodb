package services

import (
	"context"
	"temp/genproto/user"
	"temp/pkg/logger"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (u *UserService) CreateUser(ctx context.Context, req *user.User) (*user.User, error) {
	
	res, err := u.Storage.User().CreateUser(req)
	if err != nil {
		u.Logger.Error(`Error while creating User-Mongosh`, logger.Any(`Error with User`, err))
		return &user.User{}, status.Error(codes.Internal, `Couldnt create you user info`)
	}

	return res, err
}

func (u *UserService) UpdateUser(ctx context.Context, req *user.User) (*user.User, error) {
	
	res, err := u.Storage.User().UpdateUser(req)
	if err != nil {
		u.Logger.Error(`Error while Update User-Mongosh`, logger.Any(`Error with User`, err))
		return &user.User{}, status.Error(codes.Internal, `Couldnt update you user info`)
	}
	return res, err
}

func (u *UserService) GetUser(ctx context.Context, req *user.ID) (*user.User, error) {
	
	res, err := u.Storage.User().GetUser(req)
	if err != nil {
		u.Logger.Error(`Error while GETTING User-Mongosh`, logger.Any(`Error with User`, err))
		return &user.User{}, status.Error(codes.Internal, `Couldnt GETTING you user info`)
	}
	return res, err
}

func (u *UserService) DeleteUser(ctx context.Context, req *user.ID) (*user.Empty, error) {

	err := u.Storage.User().DeleteUser(req)
	if err != nil {
		u.Logger.Error(`Error while Delete User-Mongosh`, logger.Any(`Error with User`, err))
		return &user.Empty{}, status.Error(codes.Internal, `Couldnt Delete you user info`)
	}
	return &user.Empty{}, err
}