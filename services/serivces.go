package services

import (
	"temp/pkg/logger"
	"temp/storage"

	"go.mongodb.org/mongo-driver/mongo"
)
type UserService struct {
	Storage storage.IStorageI
	Logger logger.Logger
}

func NewUserService(db *mongo.Client, log logger.Logger) *UserService {
	return &UserService{
		Storage: storage.NewStoragePg(db),
		Logger: log,
	}
}