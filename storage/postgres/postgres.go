package postgres

// Mongo connection

import "go.mongodb.org/mongo-driver/mongo"

type UserRepo struct {
	Db *mongo.Client
}

func NewUserRepo(db *mongo.Client) *UserRepo {
	return &UserRepo{Db: db}
}
