package postgres

import (
	"context"
	"fmt"
	pu "temp/genproto/user"
	"time"

	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
)

func (u *UserRepo) CreateUser(user *pu.User) (*pu.User, error) {	
	
	collection := u.Db.Database("user_mongodb").Collection("users")
	ctx, cancel := context.WithTimeout(context.Background(), 5 *time.Second)
	defer cancel()
	user.Id = uuid.New().String()
	_, err := collection.InsertOne(ctx, bson.D{
		{Key: "id", Value: user.Id},
		{Key: "first_name",Value: user.FirstName},
		{Key: "last_name", Value: user.LastName},
		{Key: "email", Value: user.Email},
		{Key: "phone", Value: user.Phone},
	})
	if err != nil {
		return &pu.User{}, err
	}
	return user, err
}

func (u *UserRepo) UpdateUser(req *pu.User) (*pu.User, error) {
	
	collection := u.Db.Database("user_mongodb").Collection("users")
	ctx, cancel := context.WithTimeout(context.Background(), 5 *time.Second)
	defer cancel()

	responce, err := collection.UpdateOne(ctx, bson.M{"id": req.Id}, 
		bson.D{
			{Key: "$set", Value: bson.D{
				{Key: "email", Value: req.Email},
			}},
	})
	if err != nil {
		return &pu.User{}, err
	}
	fmt.Println(responce.ModifiedCount)
	responce, err = collection.ReplaceOne(ctx, bson.M{"id": req.Id},
		bson.M{
			"id": req.Id,
			"first_name": req.FirstName,
			"last_name": req.LastName,
			"email": req.Email,
			"phone": req.Phone,
		})
	if err != nil {
		return &pu.User{}, err
	}

	return req, nil
}

func (u *UserRepo) GetUser(req *pu.ID) (*pu.User, error) {

	collection := u.Db.Database("user_mongodb").Collection("users")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	responce := &pu.User{}
	filter := bson.D{{Key: "id", Value: req.Id}}
	err := collection.FindOne(ctx, filter).Decode(&responce)
	if err != nil {
		return &pu.User{}, err
	}

	return responce, nil
}	

func (u *UserRepo) DeleteUser(req *pu.ID) error {

	collection := u.Db.Database("user_mongodb").Collection("users")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := collection.DeleteOne(ctx, bson.M{"id" : req.Id})
	if err != nil {
		return err
	}

	return nil
}