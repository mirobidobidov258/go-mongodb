package repo

import "temp/genproto/user"

type UserStorageI interface {
	CreateUser(*user.User) (*user.User, error)
	UpdateUser(*user.User) (*user.User, error) 
	GetUser(*user.ID) (*user.User, error)
	DeleteUser(*user.ID) error
}