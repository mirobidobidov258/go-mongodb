package storage

import (
	"temp/storage/repo"

	postgres "temp/storage/postgres"
	"go.mongodb.org/mongo-driver/mongo"
)

type IStorageI interface {
	User() repo.UserStorageI
}

type StoragePg struct {
	Db 	*mongo.Client
	userRepo repo.UserStorageI
}

func NewStoragePg(db *mongo.Client) *StoragePg {
	return &StoragePg{
		Db: db,
		userRepo: postgres.NewUserRepo(db),
	}
}

func (s StoragePg) User() repo.UserStorageI {
	return s.userRepo
}
