package config

import (
	"os"
	"github.com/spf13/cast"
)

type Config struct {
	Environment string
	LogLevel 	string
	RPCPort 	string
}

func Load() Config {
	c := Config{}
	c.Environment = cast.ToString(GetOrReturnDefault("ENVIRONMENT", "develop"))

	c.LogLevel = cast.ToString(GetOrReturnDefault("LOG_LEVEL", "debug"))

	c.RPCPort = cast.ToString(GetOrReturnDefault("RPC_PORT", ":9000"))

return c
}

func GetOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}