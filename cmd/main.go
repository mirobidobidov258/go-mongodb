package main

import (
	"context"
	"net"
	"temp/config"
	"temp/pkg/db"
	"temp/pkg/logger"
	"temp/services"
	"temp/genproto/user"


	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

)

func main() {
	
	cfg := config.Load()
	
	log := logger.New(cfg.LogLevel, "go-mongodb")
	defer logger.CleanUp(log)

	connDB, err := db.ConnectToDB(cfg)
	if err != nil {
		log.Fatal(`Error while to connection with databases`, logger.Error(err))
	}
	defer connDB.Disconnect(context.Background())

	userServices := services.NewUserService(connDB, log)	

	lis, err := net.Listen("tcp", cfg.RPCPort)
	if err != nil {
		log.Fatal(`Error while to connection with TCP PROTOCOl`, logger.Error(err))
	}

	c := grpc.NewServer()
	reflection.Register(c)
	user.RegisterUserServiceServer(c, userServices)
	log.Info("Server is Running",
	logger.String("port", cfg.RPCPort))
	if err := c.Serve(lis); err != nil {
		log.Fatal(`Error while listening with TCP/UDP `, logger.Error(err))
	}
}
